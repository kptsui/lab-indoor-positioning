package org.bitbucket.kptsui.labindoorposition;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.SystemRequirementsChecker;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    public final static String TAG = "MainActivity";

    App app;
    WebView mWebView;
    Handler mHandler;

    private BeaconManager beaconManager;
    private Region region;
    private String scanId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mHandler = new Handler();
        app = App.getInstance();

        mWebView = (WebView) findViewById(R.id.mWebView);
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        mWebView.loadUrl("file:///android_asset/index.html");

        beaconManager = new BeaconManager(this);

        region = new Region("ranged region",
                UUID.fromString("B9407F30-F5F8-466E-AFF9-25556B57FE6D"), null, null);

        beaconManager.setRangingListener(new BeaconManager.RangingListener() {
            @Override
            public void onBeaconsDiscovered(Region region, final List<Beacon> beacons) {
                if(beacons.size() < 3) {
                    Log.d(TAG, "beacons size < 3, do nothing");
                    return;
                }

                Beacon b1 = beacons.get(0);
                Beacon b2 = beacons.get(1);
                Beacon b3 = beacons.get(2);

                final String invoke =
                    "javascript:updateMarker("+
                            b1.getMajor() + "," + b2.getMajor() + "," + b3.getMajor() + "," +
                            b1.getRssi() + "," + b2.getRssi() + "," + b3.getRssi() + ")";

                Log.d(TAG, invoke);

                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mWebView.loadUrl(invoke);
                    }
                });
            }
        });
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        SystemRequirementsChecker.checkWithDefaultDialogs(this);

        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                beaconManager.startRanging(region);
            }
        });
    }

    @Override
    protected void onStop() {
        beaconManager.stopRanging(region);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        beaconManager.disconnect();
    }
}
