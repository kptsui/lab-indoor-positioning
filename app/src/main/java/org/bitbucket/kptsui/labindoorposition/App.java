package org.bitbucket.kptsui.labindoorposition;

import android.app.Application;

/**
 * Created by user on 28/10/2016.
 */

public class App extends Application {
    public final static String TAG = "Application";
    private static App instance;


    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

    }

    public static App getInstance(){
        return instance;
    }
}
